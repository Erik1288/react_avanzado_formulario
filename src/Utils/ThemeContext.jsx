import {createContext} from 'react';
const initialData = {
  theme: 'dark',
};

export const ThemeContext = createContext(initialData);

const {Provider} = ThemeContext;

const ThemeProvider = ({children}) => {
  
  const toggle_theme = () => {
    document.body.classList.toggle('fondo-two');
    document.querySelector('.nav').classList.toggle('bg');
    document
      .querySelectorAll('.navbar-bran')
      .forEach((el) => el.classList.toggle('color'));
    
  };
  return <Provider value={{toggle_theme}}>{children}</Provider>;
};
export default ThemeProvider;
