import { Routes, Route } from "react-router-dom";

import { Header } from './pages/Header'
import {Informacion} from './pages/Information'
import Login from './pages/login'
import {Portada} from './pages/Portada'
import {Contacto} from './pages/Contacto'
import ThemeProvider from "./Utils/ThemeContext"


function App() {
  return (
    <ThemeProvider>
        <div>
          <Header />
          <Routes>
            <Route path='/' element={<Portada />} />
            <Route path='/Informacion' element={<Informacion />} />
            <Route path='/Login' element={<Login />} />
            <Route path='/Contacto' element={<Contacto />} />
          </Routes>
        </div>
    </ ThemeProvider>
  );
}

export default App
