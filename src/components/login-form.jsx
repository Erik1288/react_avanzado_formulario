import { Field, Form, Formik } from "formik";

const LoginForm = ({ onSubmit, innerRef }) => {
  const inicial_data = {
    user_name: "",
    password: "",
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };
  return (
    <Formik initialValues={inicial_data} onSubmit={submit} innerRef={innerRef}>
      {({ isSubmitting }) => {
        return (
          <Form>
            <div className="form-group">
              <label htmlFor="username">Nombre de Usuario</label>
              <Field
                className="form-control"
                id="username"
                type="text"
                name="user_name"
                placeholder="Ingrese su nombre de usuario"
              />
            </div>
            <div className="form-group">
              <label htmlFor="pass">Contraseña</label>
              <Field
                className="form-control"
                id="pass"
                type="password"
                name="password"
                placeholder="Ingrese su contraseña"
              />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default LoginForm;
