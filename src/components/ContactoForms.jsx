import { Field, Form, Formik } from "formik";

const ContactoForms = ({ onSubmit, innerRef }) => {
  const inicial_data = {
    user_name: "",
    password: "",
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };
  return (
    <Formik initialValues={inicial_data} onSubmit={submit} innerRef={innerRef}>
      {({ isSubmitting }) => {
        return (
          <Form>
            <div className="form-group">
              <label htmlFor="username">Nombre de Usuario</label>
              <Field
                className="form-control"
                id="username"
                type="text"
                name="user_name"
                placeholder="Ingrese su nombre de usuario"
              />
            </div>
            <div className="form-group">
              <label htmlFor="text">Contraseña</label>
              <Field
              as='textarea'
                className="form-control"
                id="text"
                name="textarea"
                placeholder="Digite su mensaje"
              />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default ContactoForms;
