import React, { useRef } from "react";
import LoginForm from "../components/login-form";

const Login = () => {
  const ref = useRef(null);

  const login = (values) => {
    console.log(values);
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-6"></div>
        <div className="col-6">
          <LoginForm onSubmit={login} innerRef={ref} />
          <button
            className="btn btn-primary"
            onClick={() => {
              ref.current.submitForm();
            }}
          >
            Login
          </button>
        </div>
      </div>
    </div>
  );
};

export default Login;
