import {useRef} from 'react';
import ContactoForms from '../components/ContactoForms';

export function Contacto() {
  const ref = useRef(null);

  const printLogin = (values) => {
    console.log(values);
  };
  return (
    <div className='container'>
      <div className='d-grid' style={{placeItems: 'center'}}>
        <ContactoForms onSubmit={printLogin} innerRef={ref} />
        <button
          className='btn btn-primary'
          onClick={() => ref.current.submitForm()}
        >
          Enviar
        </button>
      </div>
    </div>
  );
}
